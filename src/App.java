import business.Controller;
import exception.RepositoryException;
import exception.ValidatorException;
import presentation.CevaUI;
import static build.Build.*;

public class App {
    public static void main(String[] args) throws ValidatorException, RepositoryException {


//        repository in fisier
//        Controller cont = new Controller("date/users.csv", "date/friendships.csv");

//        repository in baza de date
        Controller cont = new Controller(database_url, database_user, database_password);
//        UI ui = new UI(cont);
//        ui.run();

        CevaUI ui = new CevaUI(cont);
        ui.run();
    }
}
