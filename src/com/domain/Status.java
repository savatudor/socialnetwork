package com.domain;

public enum Status {
    PENDING,
    APPROVED,
    REJECTED
}
